#### 0.0.0 (2024-01-26)

##### Other Changes

*  Create initial entry in CHANGELOG (502a1b5d)
*  Add generate-changelog command and related dependencies (61394900)
*  Refactor function to improve readability and maintainability (faa58cf1)

#### 0.0.0 (2024-01-26)

##### Other Changes

*  Refactor function to improve readability and maintainability (faa58cf1)

